public class Example5_3 {
  public static void main(String args[]) {
     CheapGood cheapGood=new CheapGood();
     cheapGood.newSetWeight(198);
     System.out.println("对象cheapGood的weight的值是:"+cheapGood.weight);
     System.out.println("cheapGood用子类新增的优惠方法计算价格："+
                         cheapGood.newGetPrice());
     cheapGood.oldSetWeight(198.987);
     System.out.println("cheapGood使用继承的方法（无优惠）计算价格："+
                          cheapGood.oldGetPrice());
  }
}
