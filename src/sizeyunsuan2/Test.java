import java.util.*;
import java.text.NumberFormat;

public class Test {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random generator = new Random();
        Scanner babala = new Scanner(System.in);

        NumberFormat fmt1 = NumberFormat.getPercentInstance();
        int level;
        String another = "y";


        int t = 0, f = 0;
        double tf = 0.0;




            System.out.println("请选择难度等级(Choose Level 1-6: )");
            level = scan.nextInt();
            if (level == 1)    //加法
            {
                System.out.println("------------------------------------------------------------");

                while (another.equalsIgnoreCase("y")) {// allows y or Y
                    int a, b, Input, Answer;
                    a = generator.nextInt(10) + 1;
                    b = generator.nextInt(10) + 1;
                    Answer = a + b;
                    System.out.println(a + "+" + b + "=?");

                    System.out.println("您的回答是?(Answer:)");
                    Input = scan.nextInt();


                    if (Input == Answer) {
                        System.out.println("回答正确!(Correctly!)");
                        t = t + 1;
                    } else {
                        System.out.println("抱歉，回答错误!(False!)");
                        f = f + 1;
                    }

                    System.out.println("正确答案是:(The Answer is :)" + Answer);
                    System.out.println("------------------------------------------------------------");

                    System.out.println("再做一题吗?继续请输入y!(Do you want another quize(y/n)?)");
                    another = babala.nextLine();
                }
            } else if (level == 2)   // 加法 减法
            {
                System.out.println("------------------------------------------------------------");
                System.out.println("准备好了吗，开始请输入y(Are you ready(y/n)?)");
                another = babala.nextLine();
                while (another.equalsIgnoreCase("y")) {// allows y or Y
                    int a, b, c, Input, Answer;
                    a = generator.nextInt(100) + 1;
                    b = generator.nextInt(100) + 1;
                    c = generator.nextInt(100) + 1;
                    Answer = a + b - c;
                    System.out.println(a + "+" + b + "-" + c + "= ?");

                    System.out.println("您的回答是?(Answer :)");
                    Input = scan.nextInt();
                    if (Input == Answer) {
                        System.out.println("回答正确!(Correctly!)");
                        t = t + 1;
                    } else {
                        System.out.println("抱歉，回答错误!(False!)");
                        f = f + 1;
                    }
                    System.out.println("正确答案是:(The Answer is :)" + Answer);
                    System.out.println("------------------------------------------------------------");
                    System.out.println("再做一题吗？继续请输入y!(Do you want another quiz(y/n)?)");
                    another = babala.nextLine();
                }
            } else if (level == 3)   // 加法 减法 乘法
            {
                System.out.println("------------------------------------------------------------");
                System.out.println("准备好了吗，开始请输入y(Are you ready(y/n)?)");
                another = babala.nextLine();
                while (another.equalsIgnoreCase("y")) {// allows y or Y
                    int a, b, c, d, Input, Answer;
                    a = generator.nextInt(10) + 1;
                    b = generator.nextInt(10) + 1;
                    c = generator.nextInt(10) + 1;
                    d = generator.nextInt(10) + 1;
                    Answer = a + b - c * d;
                    System.out.println(a + "+" + b + "-" + c + "*" + d + "= ?");

                    System.out.println("您的回答是?(Answer :)");
                    Input = scan.nextInt();
                    if (Input == Answer) {
                        System.out.println("回答正确!(Correctly!)");
                        t = t + 1;
                    } else {
                        System.out.println("抱歉，回答错误!(False!)");
                        f = f + 1;
                    }
                    System.out.println("正确答案是：(The Answer is :)" + Answer);
                    System.out.println("------------------------------------------------------------");
                    System.out.println("再做一题吗?继续请输入y!(Do you want another quiz(y/n)?)");
                    another = babala.nextLine();
                }
            } else if (level == 4) {
                System.out.println("------------------------------------------------------------");
                System.out.println("准备好了吗,开始请输入y(Are you ready(y/n)?)");
                another = babala.nextLine();
                while (another.equalsIgnoreCase("y")) {// allows y or Y
                    int a, b, c, d, k, w;
                    RationalNumber Answer;

                    a = generator.nextInt(10) + 1;
                    b = generator.nextInt(10) + 1;
                    c = generator.nextInt(10) + 1;
                    d = generator.nextInt(10) + 1;

                    RationalNumber r1 = new RationalNumber(a, b);
                    RationalNumber r2 = new RationalNumber(c, d);
                    Answer = r1.add(r2);
                    System.out.println(a + "/" + b + "+" + c + "/" + d);

                    System.out.println("分子等于?(the Answer's numer is ?)");
                    k = scan.nextInt();
                    System.out.println("分母等于?(the Answer's denom is ?)");
                    w = scan.nextInt();
                    RationalNumber haha = new RationalNumber(k, w);
                    int z = haha.compareTo(Answer);
                    // 为什么无法准确判定两个值？？？
                    // 已经解决了
                    if (z == 1) {
                        System.out.println("回答正确!(Correctly!)");
                        t = t + 1;
                    }
                    if (z == 0) {
                        System.out.println("抱歉，回答错误!(False!)");
                        f = f + 1;
                    }
                    System.out.println("正确答案是：(The Answer is :)" + Answer);
                    System.out.println("------------------------------------------------------------");
                    System.out.println("再来一题吗，继续请输入y(Do you want another quiz(y/n)?)");
                    another = babala.nextLine();
                }
            } else if (level == 5)   // 乘法
            {
                System.out.println("------------------------------------------------------------");
                System.out.println("准备好了吗，开始请输入y(Are you ready(y/n)?)");
                another = babala.nextLine();
                while (another.equalsIgnoreCase("y")) {// allows y or Y
                    int a, b, c, d, k, w, a1, b1, c1, d1;
                    RationalNumber Answer1, Answer2, Answer3;

                    a = generator.nextInt(10) + 1;
                    b = generator.nextInt(10) + 1;
                    c = generator.nextInt(10) + 1;
                    d = generator.nextInt(10) + 1;
                    a1 = generator.nextInt(10) + 1;
                    b1 = generator.nextInt(10) + 1;
                    c1 = generator.nextInt(10) + 1;
                    d1 = generator.nextInt(10) + 1;

                    RationalNumber r1 = new RationalNumber(a, b);
                    RationalNumber r2 = new RationalNumber(c, d);
                    RationalNumber r11 = new RationalNumber(a1, b1);
                    RationalNumber r22 = new RationalNumber(c1, d1);
                    Answer1 = r1.add(r2);
                    Answer2 = r11.multiply(r22);
                    Answer3 = Answer1.add(Answer2);

                    System.out.println(a + "/" + b + "+" + c + "/" + d + "+" + a1 + "/" + b1 + "*" + c1 + "/" + d1);

                    System.out.println("分子等于?(the Answer's numer is ?)");
                    k = scan.nextInt();
                    System.out.println("分母等于?(the Answer's denom is ?)");
                    w = scan.nextInt();
                    RationalNumber haha = new RationalNumber(k, w);

                    int z = haha.compareTo(Answer3);
                    // 为什么无法准确判定两个值？？？

                    if (z == 1) {
                        System.out.println("回答正确!(Correctly!)");
                        t = t + 1;
                    }
                    if (z == 0) {
                        System.out.println("抱歉，回答错误!(False!)");
                        f = f + 1;
                    }
                    System.out.println("正确答案是:(The Answer is :)" + Answer3);
                    System.out.println("------------------------------------------------------------");
                    System.out.println("再来一题吗?(Do you want another quiz(y/n)?)");
                    another = babala.nextLine();
                }
            } else if (level == 6)   // 乘法
            {
                System.out.println("------------------------------------------------------------");
                System.out.println("准备好了吗，开始请输入y(Are you ready(y/n)?)");
                another = babala.nextLine();
                while (another.equalsIgnoreCase("y")) {// allows y or Y
                    int a, b, c, d, k, w, a1, b1, c1, d1, a2, b2, c2, d2;
                    RationalNumber Answer1, Answer2, Answer3, Answer4, Answer5;

                    a = generator.nextInt(10) + 1;
                    b = generator.nextInt(10) + 1;
                    c = generator.nextInt(10) + 1;
                    d = generator.nextInt(10) + 1;
                    a1 = generator.nextInt(10) + 1;
                    b1 = generator.nextInt(10) + 1;
                    c1 = generator.nextInt(10) + 1;
                    d1 = generator.nextInt(10) + 1;
                    a2 = generator.nextInt(10) + 1;
                    b2 = generator.nextInt(10) + 1;
                    c2 = generator.nextInt(10) + 1;
                    d2 = generator.nextInt(10) + 1;

                    RationalNumber r1 = new RationalNumber(a, b);
                    RationalNumber r2 = new RationalNumber(c, d);
                    RationalNumber r11 = new RationalNumber(a1, b1);
                    RationalNumber r22 = new RationalNumber(c1, d1);
                    RationalNumber r111 = new RationalNumber(a2, b2);
                    RationalNumber r222 = new RationalNumber(c2, d2);
                    Answer1 = r1.add(r2);
                    Answer2 = r11.multiply(r22);
                    Answer3 = r111.divide(r222);    //  (r1+r2) + (r11 * Rr22) - (r111 / r222)
                    Answer4 = Answer1.add(Answer2);
                    Answer5 = Answer4.subtract(Answer3);

                    System.out.println("(" + a + "/" + b + "+" + c + "/" + d + ")" + "+" + "(" + a1 + "/" + b1 + "*" + c1 + "/" + d1 + ")" + "-" +
                            "(" + a2 + "/" + b2 + "/" + c2 + "/" + d2 + ")");

                    System.out.println("分子等于?(the Answer's numer is ?)");
                    k = scan.nextInt();
                    System.out.println("分母等于?(the Answer's denom is ?)");
                    w = scan.nextInt();
                    RationalNumber haha = new RationalNumber(k, w);

                    int z = haha.compareTo(Answer5);
                    // 为什么无法准确判定两个值？？？

                    if (z == 1) {
                        System.out.println("回答正确!(Correctly!)");
                        t = t + 1;
                    }
                    if (z == 0) {
                        System.out.println("抱歉，回答错误!(False!)");
                        f = f + 1;
                    }
                    System.out.println("正确答案是：（The Answer is :）" + Answer5);
                    System.out.println("------------------------------------------------------------");
                    System.out.println("再来一题?继续请输入y(Do you want another quiz(y/n))?");
                    another = babala.nextLine();
                }
            } else if (level <= 0 || level > 6) {
                System.out.println("请输入难度等级范围内的值(please enter the values within the difficuty level)");
            }






        System.out.println("************************************************************");

        t = t;
        f = f;
        tf = (double) t / (t + f);
        if ((t + f) != 0) {
            System.out.println("回答正确的题目数(Correct answer)：" + t);
            System.out.println("回答错误的题目数(False answer)：" + f);
            System.out.println("您的准确率为(Accuracy)：" + fmt1.format(tf));
        }
        if ((t + f) == 0) {
            System.out.println("未回答任何问题(You haven't answer any question)");
        }
    }


}
