import java.util.Scanner;
import java.lang.Integer;

public class Caesar {
    public static void main(String[] args){
        System.out.println("请输入明文：");
        Scanner input=new Scanner(System.in);
        String s=input.nextLine();
        System.out.println("请输入密钥：");
        Scanner output=new Scanner(System.in);
        int key=output.nextInt();
        Encryption(s,key);
    }
    public static void Encryption(String str,int k) {
        String string ="";
        for(int i=0;i<str.length();i++){
            char c=str.charAt(i);
            if(c>='a'&&c<='z'){
                c+=k%26;
                if(c<'a') {
                    c+=26;
                }
                if(c>'z') {
                    c-=26;
                }
            }else if(c>='A'&&c<='Z'){
                c+=k%26;
                if(c<'A') {
                    c+=26;
                }
                if(c>'Z') {
                    c-=26;
                }
            }
            string+=c;
        }
        System.out.println(str+"加密后为："+string);
    }
}
